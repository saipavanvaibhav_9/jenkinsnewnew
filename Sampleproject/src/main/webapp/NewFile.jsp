<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <link rel="stylesheet" href="NewFile.css">
    <title>shoploginpage</title>
        <link rel="icon" href="https://th.bing.com/th/id/R.f85f50763e82aebba5baa290fa51e265?rik=43G0l3AMlwfnhw&riu=http%3a%2f%2fwww.pngmart.com%2ffiles%2f7%2fCart-PNG-Background-Image.png&ehk=IY2HqN0Zi8MoidlsUhTsOmoEoMsyWDUY7KjlrNXW4pQ%3d&risl=&pid=ImgRaw&r=0" type="image/x-icon">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body class = "form changeColors ">


  <div class="z ">
    <h2 class="login ">Login Page</h2>

    <form class="p-3 form" >
        <div>
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-envelope-fill" viewBox="0 0 16 16">
                <path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z"/>
            </svg>
            <label for="email">Email</label>
            <input id="email" class="form-control" type="email" placeholder="Enter your email-ID">        
        </div>
        <div>
            <label for="password">Password</label>
            <input id="password" class="form-control" type="password" placeholder="Enter your password">        
        </div>
        <button class="mt-3" onclick="login(event)">Login</button>
        <button class = "mt-3" onclick="signinpage(event)" >Signup</button>
    </form>

  </div>

  <!-- Your HTML code remains unchanged -->
  
  
  

<script>
    function login(event) {
        event.preventDefault();
  
        const emailchecking = encodeURIComponent(document.getElementById('email').value);
        const passwordchecking = encodeURIComponent(document.getElementById('password').value);
     
        fetch("http://localhost:9999/shop/login?email=" + emailchecking + "&password=" + passwordchecking,
            {
                method: 'get',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(response => response.json())
            .then(data => {
                if (data.success) {
                    window.location.href = 'http://localhost:8080/Sampleproject/Welcome.jsp';
                } else {
                    alert('Wrong credentials. Please try again.');
                }
            })
            .catch(error => {
                console.error('Error during login:', error);
                alert('An error occurred during login. Please try again later.');
            });
    }

    function signinpage(event) {
    	event.preventDefault();
        // Display the signup form, show a hidden form or modal
        // You should include the form structure directly in your HTML
        window.location.href = 'http://localhost:8080/Sampleproject/Signup.jsp';
        alert("redirecting to signup page")
     <!--   console.log("insingup page");
        window.location.href = 'http://localhost:8080/Sampleproject/Signup.jsp';
        console.log("after");
               -->
    }


</script>

</body>
</html>
